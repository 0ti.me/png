import { ByteArray } from '../../submodules/bitwise/src/byte-array';
import { BitGrayscalePngImplementation } from '../../src/bit-grayscale-png-implementation';
import { d, expect } from '@0ti.me/ts-test-deps';
import { validateAndCompareChunks } from '../tools/validate-chunks';
import * as zlib from 'zlib';

const me = __filename;

d(me, () => {
  let impl: BitGrayscalePngImplementation;
  let makeCheckerboard: any;
  let nextVal: any;
  let valsIndex: number;
  let vals: number[];

  beforeEach(() => {
    impl = new BitGrayscalePngImplementation();

    /* compression makes testing harder and is mostly just testing zlib */
    impl.compressionLevel = zlib.constants.Z_NO_COMPRESSION;

    /* not really a checkerboard if more than 2 vals though */
    makeCheckerboard = (rows: number, cols: number): number[][] => {
      return new Array(rows).fill(0).map(() => {
        const result = new Array(cols).fill(0).map(() => nextVal());

        if (cols % 2 === 0) {
          nextVal(); // force an alternation of value
        }

        return result;
      });
    };

    nextVal = (): number => {
      const next = vals[valsIndex];

      valsIndex = (valsIndex + 1) % 2;

      return next;
    };

    /* override me probably for > 1 bit depth */
    vals = [0x01, 0x00];
    valsIndex = 0;
  });

  describe('property get bitDepth', () => {
    it('should default bit depth to 1', () =>
      expect(impl).to.have.property('bitDepth', 1));
  });

  describe('given a simple 32x3 1-bit image', () => {
    beforeEach(() => {
      impl.bitDepth = 1;
      impl.image = makeCheckerboard(3, 32);
      impl.margin = 8;
      impl.marginColor = { colorNumber: 0x01 };
      impl.scale = 1;
    });

    it('(getPngBuffer) should produce a reasonable png buffer', () =>
      validateAndCompareChunks(impl.getPngBuffer(), {
        chunkHeader: Buffer.from(
          /* prettier-ignore */
          [
            '0000000d',               /* chunk length 13 */
            '49484452',               /* chunk type IHDR */
            '0000003000000013',       /* image width, then height */
            '0100000000',             /* image bit depth, color type, compression, filter, and interlace methods */
            'fe41b3b2',               /* chunk CRC */
          ].join(''),
          'hex',
        ),
        chunkData: Buffer.from(
          /* prettier-ignore */
          [
            '00000090',               /* chunk length 144 */
            '49444154',               /* chunk type IDAT */
            '78010185007aff',         /* zlib compression data part 1 */
            '00ffffffffffff'          /* scanlines 1:8 */
              .repeat(8),
            '00ffaaaaaaaaff',         /* scanline 9 */
            '00ff55555555ff',         /* scanline 10 */
            '00ffaaaaaaaaff',         /* scanline 11 */
            '00ffffffffffff'          /* scanlines 12:19 */
              .repeat(8),
            '20446c3f',               /* zlib CRC of uncompressed data */
            '3af515d0',               /* chunk CRC */
          ].join(''),
          'hex',
        ),
      }));

    it('(getValue) should still give a masked value even if mask is undefined', () => {
      const val = (impl as any).getValue(0, 0);

      impl.image[0][0] = 0xffffffff >>> 0;

      expect((impl as any).getValue(0, 0))
        .to.equal(val)
        .and.to.equal(0x01);
    });

    describe('given a scale > 1', () => {
      beforeEach(() => {
        impl.scale = 2;
      });

      it('(getPngBuffer) should produce a reasonable png buffer with a scaled margin', () =>
        validateAndCompareChunks(impl.getPngBuffer(), {
          chunkHeader: Buffer.from(
            /* prettier-ignore */
            [
            '0000000d',               /* chunk length 13 */
            '49484452',               /* chunk type IHDR */
            '0000006000000026',       /* image width, then height */
            '0100000000',             /* image bit depth, color type, compression, filter, and interlace methods */
            '13e2a3e4',               /* chunk CRC */
            ].join(''),
            'hex',
          ),
          chunkData: Buffer.from(
            /* prettier-ignore */
            [
              '000001f9',                   /* chunk length 144 */
              '49444154',                   /* chunk type IDAT */
              '780101ee0111fe',             /* zlib compression data part 1 */
              '00ffffffffffffffffffffffff'  /* scanlines 1:16 */
                .repeat(16),
              '00ffffccccccccccccccccffff', /* scanline 19 */
              '00ffffccccccccccccccccffff', /* scanline 20 */
              '00ffff3333333333333333ffff', /* scanline 19 */
              '00ffff3333333333333333ffff', /* scanline 20 */
              '00ffffccccccccccccccccffff', /* scanline 21 */
              '00ffffccccccccccccccccffff', /* scanline 22 */
              '00ffffffffffffffffffffffff'  /* scanlines 23:38 */
                .repeat(16),
              'e6a3b328',                   /* zlib CRC of uncompressed data */
              '4a96315e',                   /* chunk CRC */
            ].join(''),
            'hex',
          ),
        }));
    });
  });

  describe('given a simple 3x3 1-bit image', () => {
    beforeEach(() => {
      impl.image = makeCheckerboard(3, 3);
    });

    it('(getPngBuffer) should produce a reasonable png buffer', () =>
      validateAndCompareChunks(impl.getPngBuffer(), {
        chunkHeader: Buffer.from(
          /* prettier-ignore */
          [
            '0000000d',         /* chunk length 13 */
            '49484452',         /* chunk type IHDR */
            '0000000300000003', /* image width, then height */
            '0100000000',       /* image bit depth, color type, compression, filter, and interlace methods */
            '7e538812',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
        chunkData: Buffer.from(
          /* prettier-ignore */
          [
            '00000011',         /* chunk length 17 */
            '49444154',         /* chunk type IDAT */
            '7801010600f9ff',   /* zlib compression data part 1 */
            '00a0',             /* scanline 1 */
            '0040',             /* scanline 2 */
            '00a0',             /* scanline 3 */
            '04860181',         /* zlib CRC of uncompressed data */
            'e6a8034f',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
      }));
  });

  describe('given a long margin', () => {
    beforeEach(() => {
      vals = [0x00, 0x01];
      impl.image = makeCheckerboard(1, 1);
      impl.margin = 33;
      impl.marginColor = { colorNumber: 0x01 };
    });

    it('(getPngBuffer) should produce a reasonable png buffer', () =>
      validateAndCompareChunks(impl.getPngBuffer(), {
        chunkHeader: Buffer.from(
          /* prettier-ignore */
          [
            '0000000d',             /* chunk length 13 */
            '49484452',             /* chunk type IHDR */
            '0000004300000043',     /* image width, then height */
            '0100000000',           /* image bit depth, color type, compression, filter, and interlace methods */
            'efb185de',             /* chunk CRC */
          ].join(''),
          'hex',
        ),
        chunkData: Buffer.from(
          /* prettier-ignore */
          [
            '000002a9',             /* chunk length 681 */
            '49444154',             /* chunk type IDAT */
            '7801019e0261fd',       /* zlib compression data part 1 */
            '00ffffffffffffffffe0'  /* scanlines 1:32 */
              .repeat(33),
            '00ffffffffbfffffffe0', /* scanline 33 */
            '00ffffffffffffffffe0'  /* scanlines 1:2 */
              .repeat(33),
            '5eb35067',             /* zlib CRC of uncompressed data */
            'c04b59f9',             /* chunk CRC */
          ].join(''),
          'hex',
        ),
      }));
  });

  describe('given a reasonable buffer', () => {
    beforeEach(() => {
      // use this to build a buffer
      const byteArray: ByteArray = new ByteArray();
      let i: number;
      let j: number;

      for (i = 0; i < 20; ++i) {
        for (j = 0; j < i; ++j) {
          byteArray.addUInt8(0x00);
        }

        for (j = 0; j < i; ++j) {
          byteArray.addUInt8(0x01);
        }
      }

      impl.height = 19;
      impl.width = 20;

      impl.image = byteArray.buffer;
    });

    it('(getPngBuffer) should produce a reasonable png buffer', () =>
      validateAndCompareChunks(impl.getPngBuffer(), {
        chunkHeader: Buffer.from(
          /* prettier-ignore */
          [
            '0000000d',         /* chunk length 13 */
            '49484452',         /* chunk type IHDR */
            '0000001400000013', /* image width, then height */
            '0100000000',       /* image bit depth, color type, compression, filter, and interlace methods */
            'b8f71018',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
        chunkData: Buffer.from(
          /* prettier-ignore */
          [
            '00000057',         /* chunk length 87 */
            '49444154',         /* chunk type IDAT */
            '7801014c00b3ff',   /* zlib compression data part 1 */
            /* 0100 1100 0111 0000 1111 0000 */
            '004c70f0',         /* scanline 0 */
            '0007c0f0',         /* scanline 1 */
            '00c07f00',         /* scanline 2 */
            '000ff000',         /* scanline 3 */
            '007fc000',         /* scanline 4 */
            '00ffc000',         /* scanline 5 */
            '007ff000',         /* scanline 6 */
            '000fff00',         /* scanline 7 */
            '00007ff0',         /* scanline 8 */
            '00c000f0',         /* scanline 9 */
            '00ffc000',         /* scanline 10 */
            '0007fff0',         /* scanline 11 */
            '000000f0',         /* scanline 12 */
            '00fff000',         /* scanline 13 */
            '00007ff0',         /* scanline 14 */
            '00fc0000',         /* scanline 15 */
            '000ffff0',         /* scanline 16 */
            '00c00000',         /* scanline 17 */
            '007ffff0',         /* scanline 18 */
            '1d291c67',         /* zlib CRC of uncompressed data */
            '440a8cff',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
      }));

    describe('given level 9 compression', () => {
      beforeEach(() => {
        impl.compressionLevel = 9;
      });

      it('(getPngBuffer) should produce a reasonable png buffer', () =>
        validateAndCompareChunks(impl.getPngBuffer(), {
          chunkHeader: Buffer.from(
            /* prettier-ignore */
            [
            '0000000d',         /* chunk length 13 */
            '49484452',         /* chunk type IHDR */
            '0000001400000013', /* image width, then height */
            '0100000000',       /* image bit depth, color type, compression, filter, and interlace methods */
            'b8f71018',         /* chunk CRC */
          ].join(''),
            'hex',
          ),
          chunkData: Buffer.from(
            /* prettier-ignore */
            [
            '0000003b',         /* chunk length 59 */
            '49444154',         /* chunk type IDAT */
            '78da258ab10d00',   /* zlib compression data part 1 */
            '3008c3bc31f6b1',   /* arbitrary compressed data 1 */
            '1ed30f788d9372',   /* arbitrary compressed data 2 */
            '40250a7488e438',   /* arbitrary compressed data 3 */
            '611f6121c26109',   /* arbitrary compressed data 4 */
            '3c202b5ebc9281',   /* arbitrary compressed data 5 */
            '40e32c4b36ebfb',   /* arbitrary compressed data 6 */
            '4b7f7aaf9a7a',     /* arbitrary compressed data 7 */
            '1d291c67',         /* zlib CRC of uncompressed data */
            '263ad563',         /* chunk CRC */
          ].join(''),
            'hex',
          ),
        }));
    });
  });

  describe('given a bit depth of 2', () => {
    beforeEach(() => {
      /* prettier-ignore */
      vals = [
        /* note we can put anything the first 6 bits because it gets masked out */
        0xfc /* zero */,
        0xfd /* one */,
        0xfe /* two */,
        0xff /* three */,
      ];
      impl.bitDepth = 2;
      impl.image = [
        [vals[0], vals[1], vals[2], vals[3]],
        [vals[1], vals[2], vals[3], vals[0]],
        [vals[2], vals[3], vals[0], vals[1]],
        [vals[3], vals[0], vals[1], vals[2]],
      ];
    });

    it('(getPngBuffer) should produce a reasonable png buffer', () =>
      validateAndCompareChunks(impl.getPngBuffer(), {
        chunkHeader: Buffer.from(
          /* prettier-ignore */
          [
            '0000000d',         /* chunk length 13 */
            '49484452',         /* chunk type IHDR */
            '0000000400000004', /* image width, then height */
            '0200000000',       /* image bit depth, color type, compression, filter, and interlace methods */
            'c62ad903',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
        chunkData: Buffer.from(
          /* prettier-ignore */
          [
            '00000013',         /* chunk length 19 */
            '49444154',         /* chunk type IDAT */
            '7801010800f7ff',   /* zlib compression data part 1 */
            '001b',             /* scanline 1 */
            '006c',             /* scanline 2 */
            '00b1',             /* scanline 3 */
            '00c6',             /* scanline 4 */
            '05ba01ff',         /* zlib CRC of uncompressed data */
            'cdc913b3',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
      }));
  });

  describe('given a bit depth of 4', () => {
    beforeEach(() => {
      /* prettier-ignore */
      vals = [
        /* note we can put anything the first 4 bits because it gets masked out */
        0xf0 /* zero */,
        0xf1 /* one */,
        0xf2 /* two */,
        0xf3 /* three */,
        0xf4 /* four */,
        0xf5 /* five */,
        0xf6 /* six */,
        0xf7 /* seven */,
      ];
      impl.bitDepth = 4;
      /* prettier-ignore */
      impl.image = [
        [vals[0], vals[1], vals[2], vals[3], vals[4], vals[5], vals[6], vals[7]],
        [vals[1], vals[2], vals[3], vals[4], vals[5], vals[6], vals[7], vals[0]],
        [vals[2], vals[3], vals[4], vals[5], vals[6], vals[7], vals[0], vals[1]],
        [vals[3], vals[4], vals[5], vals[6], vals[7], vals[0], vals[1], vals[2]],
        [vals[4], vals[5], vals[6], vals[7], vals[0], vals[1], vals[2], vals[3]],
        [vals[5], vals[6], vals[7], vals[0], vals[1], vals[2], vals[3], vals[4]],
        [vals[6], vals[7], vals[0], vals[1], vals[2], vals[3], vals[4], vals[5]],
        [vals[7], vals[0], vals[1], vals[2], vals[3], vals[4], vals[5], vals[6]],
      ];
    });

    it('(getPngBuffer) should produce a reasonable png buffer', () =>
      validateAndCompareChunks(impl.getPngBuffer(), {
        chunkHeader: Buffer.from(
          /* prettier-ignore */
          [
            '0000000d',         /* chunk length 13 */
            '49484452',         /* chunk type IHDR */
            '0000000800000008', /* image width, then height */
            '0400000000',       /* image bit depth, color type, compression, filter, and interlace methods */
            '24940c56',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
        chunkData: Buffer.from(
          /* prettier-ignore */
          [
            '00000033',         /* chunk length 51 */
            '49444154',         /* chunk type IDAT */
            '7801012800d7ff',   /* zlib compression data part 1 */
            '0001234567',       /* scanline 1 */
            '0012345670',       /* scanline 2 */
            '0023456701',       /* scanline 3 */
            '0034567012',       /* scanline 4 */
            '0045670123',       /* scanline 5 */
            '0056701234',       /* scanline 6 */
            '0067012345',       /* scanline 7 */
            '0070123456',       /* scanline 8 */
            '92900771',         /* zlib CRC of uncompressed data */
            '48f651ba',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
      }));
  });
});
