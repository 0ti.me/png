import { d, expect } from '@0ti.me/ts-test-deps';
import { getRandomInt, getRandomEntry } from '../tools/random';
import { IColor } from '../../src/color';
import { SharedPngImplementation } from '../../src/shared-png-implementation';
import * as zlib from 'zlib';

const me = __filename;

let mockBitDepth: number;
let mockBpp: number;
let mockColorType: number;
let mockFilterMethod: number;
let mockInterlaceMethod: number;
let mockHeight: number;
let mockMarginColor: number;
let mockPngBuffer: Buffer;
let mockScale: number;
let mockWidth: number;
let mockWrittenDataChunkBufferLength: number;

class MockImpl extends SharedPngImplementation {
  public get bitDepth(): number {
    return mockBitDepth;
  }

  public get bpp(): number {
    return mockBpp;
  }

  public get colorType(): number {
    return mockColorType;
  }

  public get filterMethod(): number {
    return mockFilterMethod;
  }

  public get interlaceMethod(): number {
    return mockInterlaceMethod;
  }

  public get height(): number {
    return mockHeight;
  }

  public get marginColor(): IColor {
    return { colorNumber: mockMarginColor };
  }

  public get scale(): number {
    return mockScale;
  }

  public get width(): number {
    return mockWidth;
  }

  public get writtenDataChunkBufferLength(): number {
    return mockWrittenDataChunkBufferLength;
  }

  public getPngBuffer(): Buffer {
    return mockPngBuffer;
  }

  public fillData(): SharedPngImplementation {
    return this;
  }
}

d(me, 'given an implementation', () => {
  let impl: MockImpl;

  beforeEach(() => {
    mockBitDepth = getRandomEntry<number>([
      1, 2, 4, 8, 16,
    ]); /* a valid bit depth */
    mockBpp = getRandomInt(256);
    mockColorType = getRandomInt(256);
    mockHeight = (getRandomInt(40) + 1) * 4 + 17;
    mockFilterMethod = getRandomInt(256);
    mockInterlaceMethod = getRandomInt(256);
    mockMarginColor = getRandomInt(0xffffffff >>> 0);
    mockPngBuffer = Buffer.from('abcdef', 'hex'); /* some png buffer */
    mockScale = getRandomInt(3) + 1; /* 1 - 3 */
    mockWidth = mockHeight; /* random QR code width */

    impl = new MockImpl();
  });

  describe('set property: compressionLevel', () => {
    describe('given a compression level set to zero', () => {
      beforeEach(() => {
        impl.compressionLevel = 0;
      });

      it('should provide no compression', () =>
        expect(impl).to.have.property(
          'compressionLevel',
          zlib.constants.Z_NO_COMPRESSION,
        ));
    });

    describe('given a compression level set to one', () => {
      beforeEach(() => {
        impl.compressionLevel = 1;
      });

      it('should provide best speed compression', () =>
        expect(impl).to.have.property(
          'compressionLevel',
          zlib.constants.Z_BEST_SPEED,
        ));
    });

    describe('given a compression level set to Z_NO_COMPRESSION', () => {
      beforeEach(
        () => (impl.compressionLevel = zlib.constants.Z_NO_COMPRESSION),
      );

      it('should provide no compression', () =>
        expect(impl).to.have.property(
          'compressionLevel',
          zlib.constants.Z_NO_COMPRESSION,
        ));
    });

    describe('given a compression level set to Z_BEST_SPEED', () => {
      beforeEach(() => (impl.compressionLevel = zlib.constants.Z_BEST_SPEED));

      it('should provide best speed compression for Z_BEST_SPEED', () =>
        expect(impl).to.have.property(
          'compressionLevel',
          zlib.constants.Z_BEST_SPEED,
        ));
    });

    describe('given a compression level set with a number -1 through 9', () => {
      it('should provide that as the compression level', () =>
        new Array(11)
          .fill(-1)
          .map((ea, i) => ea + i)
          .forEach((goodValue) => {
            impl.compressionLevel = goodValue;

            expect(impl).to.have.property('compressionLevel', goodValue);
          }));
    });

    describe('given a compression level set to two', () => {
      beforeEach(() => (impl.compressionLevel = 2));

      it('should provide best compression level', () =>
        expect(impl).to.have.property('compressionLevel', 2));
    });

    describe('given a compression level set to Z_BEST_COMPRESSION', () => {
      beforeEach(
        () => (impl.compressionLevel = zlib.constants.Z_BEST_COMPRESSION),
      );

      it('should provide best compression', () =>
        expect(impl).to.have.property(
          'compressionLevel',
          zlib.constants.Z_BEST_COMPRESSION,
        ));
    });

    describe('given a compression level set to three', () => {
      beforeEach(() => (impl.compressionLevel = 3));

      it('should provide the default compression', () =>
        expect(impl).to.have.property('compressionLevel', 3));
    });

    describe('given a compression level set to Z_DEFAULT_COMPRESSION', () => {
      beforeEach(
        () => (impl.compressionLevel = zlib.constants.Z_DEFAULT_COMPRESSION),
      );

      it('should provide the default compression', () =>
        expect(impl).to.have.property(
          'compressionLevel',
          zlib.constants.Z_DEFAULT_COMPRESSION,
        ));
    });

    describe('given a compression level unset', () => {
      it('should provide the default compression', () =>
        expect(impl).to.have.property(
          'compressionLevel',
          zlib.constants.Z_DEFAULT_COMPRESSION,
        ));
    });

    describe('given a compression level set to undefined', () => {
      beforeEach(() => (impl.compressionLevel = undefined));

      it('should provide the default compression', () =>
        expect(impl).to.have.property(
          'compressionLevel',
          zlib.constants.Z_DEFAULT_COMPRESSION,
        ));
    });

    it('should throw with an invalid compression setting', () =>
      [-2, 10].forEach((badValue) =>
        expect(() => (new MockImpl().compressionLevel = badValue)).to.throw(),
      ));
  });

  describe('given a bit depth of 1', () => {
    beforeEach(() => {
      mockBitDepth = 1;
    });

    it('(get property bitDepthFactor) should have a bit depth factor of 1/8', () =>
      expect(new MockImpl()).to.have.property('bitDepthFactor', 0.125));

    describe('given a height of 12, a scale of 2, and a width of 10', () => {
      beforeEach(() => {
        mockHeight = 12;
        mockScale = 2;
        mockWidth = 10;
      });

      it('should have an uncompressed data chunk buffer length of 119', () =>
        expect(new MockImpl()).to.have.property(
          'uncompressedDataChunkBufferLength',
          119,
        ));
    });

    describe('given a height of 8, a scale of 3, and a a width of 11', () => {
      beforeEach(() => {
        mockHeight = 8;
        mockScale = 3;
        mockWidth = 11;
      });

      it('should have an uncompressed data chunk buffer length of 167', () =>
        expect(new MockImpl()).to.have.property(
          'uncompressedDataChunkBufferLength',
          167,
        ));
    });
  });

  describe('given a bit depth of 8', () => {
    beforeEach(() => {
      mockBitDepth = 8;
    });

    it('(get property bitDepthFactor) should have a bit depth factor of 1', () =>
      expect(new MockImpl()).to.have.property('bitDepthFactor', 1));

    describe('given a height of 14, a scale of 2, and a width of 13', () => {
      beforeEach(() => {
        mockHeight = 14;
        mockScale = 2;
        mockWidth = 13;
      });

      it('should have an uncompressed data chunk buffer of lots', () =>
        expect(new MockImpl()).to.have.property(
          'uncompressedDataChunkBufferLength',
          779,
        ));
    });
  });

  it('(get property pngMagicChunkBufferLength) should have a length of 8', () =>
    expect(new MockImpl()).to.have.property('pngMagicChunkBufferLength', 8));

  it('(get property pngMagicChunkBufferStartOffset) should have a value of 0', () =>
    expect(new MockImpl()).to.have.property(
      'pngMagicChunkBufferStartOffset',
      0,
    ));

  it('(get property headerChunkBufferStartOffset) should have a value of 8', () =>
    expect(new MockImpl()).to.have.property('headerChunkBufferStartOffset', 8));

  it('(get property headerChunkBufferLength) should have a value of 25', () =>
    expect(new MockImpl()).to.have.property('headerChunkBufferLength', 25));

  it('(get property dataChunkBufferStartOffset) should have a value of 33', () => {
    const impl = new MockImpl();

    expect(impl)
      .to.have.property('dataChunkBufferStartOffset', 33)
      .and.to.equal(
        (impl as any).headerChunkBufferStartOffset +
          (impl as any).headerChunkBufferLength,
      );
  });
});
