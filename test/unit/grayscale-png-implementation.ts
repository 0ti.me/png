import { d, expect, sinon } from '@0ti.me/ts-test-deps';
import { GrayscalePngImplementation } from '../../src/grayscale-png-implementation';
import { SinonStub } from 'sinon';

const me = __filename;

import { getRandomEntry } from '../tools/random';

let fillDataStub: SinonStub;
let mockDefaultBitDepth: number;
let mockValidBitDepths: number[];

class MockImpl extends GrayscalePngImplementation {
  public get defaultBitDepth(): number {
    return mockDefaultBitDepth;
  }

  public get validBitDepths(): number[] {
    return mockValidBitDepths;
  }

  public fillData(buffer: Buffer, offset: number): MockImpl {
    fillDataStub(buffer, offset);

    return this;
  }
}

d(me, () => {
  let impl: MockImpl;

  beforeEach(() => {
    fillDataStub = sinon.stub();
    mockValidBitDepths = new Array(17).fill(0).map((ea, i) => ea + i);
    mockDefaultBitDepth = getRandomEntry(mockValidBitDepths);
    impl = new MockImpl();
  });

  describe('given a 5x7 scale 2 rectangular image', () => {
    beforeEach(() => {
      impl.height = 7;
      impl.scale = 2;
      impl.width = 5;
    });

    it('should have the right height value', () =>
      expect(impl).to.have.property('height', 7));

    it('should have the right scale value', () =>
      expect(impl).to.have.property('scale', 2));

    it('should have the right width value', () =>
      expect(impl).to.have.property('width', 5));

    it('(getSourceValue) should error if you try to access a value outside the width range', () =>
      expect(() => (impl as any).getSourceValue(5, 0)).to.throw());

    it('(getSourceValue) should error if you try to access a value outside the height range', () =>
      expect(() => (impl as any).getSourceValue(0, 7)).to.throw());
  });

  describe('property get bpp', () => {
    it('should return appropriate bpp values', () =>
      [
        [1, 1],
        [2, 1],
        [4, 1],
        [8, 1],
        [16, 2],
      ].forEach(([bd, bpp]) => {
        impl.bitDepth = bd;

        expect(impl, `for bitDepth ${bd}`).to.have.property('bpp', bpp);
      }));
  });
});
