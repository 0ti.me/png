import { ByteGrayscalePngImplementation } from '../../src/byte-grayscale-png-implementation';
import { d, expect } from '@0ti.me/ts-test-deps';
import { validateAndCompareChunks } from '../tools/validate-chunks';
import * as zlib from 'zlib';

const me = __filename;

d(me, () => {
  let impl: ByteGrayscalePngImplementation;

  beforeEach(() => {
    impl = new ByteGrayscalePngImplementation();

    impl.scaleMargin =
      false; /* these tests were developed assuming a non-scaled margin */
  });

  it('should error if you try to access the written data chunk buffer length before initializing it', () =>
    expect(() => (impl as any).writtenDataChunkBufferLength).to.throw());

  describe('property get bitDepth', () => {
    it('should default bit depth to 8', () =>
      expect(impl).to.have.property('bitDepth', 8));
  });

  describe('property set bitDepth', () => {
    it('should throw for bad values', () =>
      new Array(26)
        .fill(-1)
        .map((ea, i) => ea + i)
        .filter((ea) => ea !== 8 && ea !== 16)
        .forEach((badValue) =>
          expect(
            () => (impl.bitDepth = badValue),
            `did not throw for ${badValue}`,
          ).to.throw(),
        ));
  });

  describe('given a bit depth of 16', () => {
    beforeEach(() => (impl.bitDepth = 16));

    it('(property get bitDepth) should have a bit depth of 16', () =>
      expect(impl).to.have.property('bitDepth', 16));
  });

  describe('given a 2x2 rectangular grayscale image', () => {
    beforeEach(() => {
      impl.compressionLevel = zlib.constants.Z_NO_COMPRESSION;
      impl.height = 2;
      impl.image = Buffer.from('084080f0', 'hex');
      impl.width = 2;
    });

    it('(getPngBuffer) should build and return a valid png buffer', () =>
      validateAndCompareChunks(impl.getPngBuffer(), {
        chunkHeader: Buffer.from(
          /* prettier-ignore */
          [
            '0000000d',         /* chunk length 13 */
            '49484452',         /* chunk type IHDR */
            '0000000200000002', /* image width, then height */
            '0800000000',       /* image bit depth, color type, compression, filter, and interlace methods */
            '57dd52f8',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
        chunkData: Buffer.from(
          /* prettier-ignore */
          [
            '00000011',         /* chunk length 17 */
            '49444154',         /* chunk type IDAT */
            '7801010600f9ff',   /* zlib compression data part 1 */
            '000840',           /* scanline 1 */
            '0080f0',           /* scanline 2 */
            '031e01b9',         /* zlib CRC of uncompressed data */
            'b4342653',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
      }));

    it('(getValue) should return the correct value with a negative coordinates', () =>
      expect((impl as any).getValue(-1, -1)).to.equal(0xf0));

    describe('given scaled to 2', () => {
      beforeEach(() => {
        impl.scale = 2;
      });

      it('(getPngBuffer) should build and return a valid png buffer', () =>
        validateAndCompareChunks(impl.getPngBuffer(), {
          chunkHeader: Buffer.from(
            /* prettier-ignore */
            [
              '0000000d',         /* chunk length 13 */
              '49484452',         /* chunk type IHDR */
              '0000000400000004', /* image width, then height */
              '0800000000',       /* image bit depth, color type, compression, filter, and interlace methods */
              '8c9ac1a2',         /* chunk CRC */
            ].join(''),
            'hex',
          ),
          chunkData: Buffer.from(
            /* prettier-ignore */
            [
              '0000001f',         /* chunk length 31 */
              '49444154',         /* chunk type IDAT */
              '7801011400ebff',   /* zlib compression data part 1 */
              '0008084040',       /* scanline 1 */
              '0008084040',       /* scanline 2 */
              '008080f0f0',       /* scanline 3 */
              '008080f0f0',       /* scanline 4 */
              '2b1406e1',         /* zlib CRC of uncompressed data */
              '3cce3fd9',         /* chunk CRC */
            ].join(''),
            'hex',
          ),
        }));
    });

    describe('given a margin of 3', () => {
      beforeEach(() => {
        impl.margin = 3;
        impl.marginColor = { colorNumber: 0xf2 };
      });

      it('(getPngBuffer) should build and return a valid png buffer', () =>
        validateAndCompareChunks(impl.getPngBuffer(), {
          chunkHeader: Buffer.from(
            /* prettier-ignore */
            [
              '0000000d',           /* chunk length 13 */
              '49484452',           /* chunk type IHDR */
              '0000000800000008',   /* image width, then height */
              '0800000000',         /* image bit depth, color type, compression, filter, and interlace methods */
              'e164e157',           /* chunk CRC */
            ].join(''),
            'hex',
          ),
          chunkData: Buffer.from(
            /* prettier-ignore */
            [
              '00000053',           /* chunk length 83 */
              '49444154',           /* chunk type IDAT */
              '7801014800b7ff',     /* zlib compression data part 1 */
              '00f2f2f2f2f2f2f2f2', /* scanline 1 */
              '00f2f2f2f2f2f2f2f2', /* scanline 2 */
              '00f2f2f2f2f2f2f2f2', /* scanline 3 */
              '00f2f2f20840f2f2f2', /* scanline 4 */
              '00f2f2f280f0f2f2f2', /* scanline 5 */
              '00f2f2f2f2f2f2f2f2', /* scanline 6 */
              '00f2f2f2f2f2f2f2f2', /* scanline 7 */
              '00f2f2f2f2f2f2f2f2', /* scanline 8 */
              '32f83a71',           /* zlib CRC of uncompressed data */
              '5f7757db',           /* chunk CRC */
            ].join(''),
            'hex',
          ),
        }));
    });
  });

  describe('given an 8-bit image 2d array', () => {
    beforeEach(() => {
      impl.bitDepth = 8;
      impl.compressionLevel = zlib.constants.Z_NO_COMPRESSION;
      impl.height = 2;
      impl.width = 2;

      impl.image = [
        [0x08, 0x40],
        [0x80, 0xf0],
      ];
    });

    it('(getPngBuffer) should build and return a valid png buffer', () =>
      validateAndCompareChunks(impl.getPngBuffer(), {
        chunkHeader: Buffer.from(
          /* prettier-ignore */
          [
            '0000000d',         /* chunk length 13 */
            '49484452',         /* chunk type IHDR */
            '0000000200000002', /* image width, then height */
            '0800000000',       /* image bit depth, color type, compression, filter, and interlace methods */
            '57dd52f8',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
        chunkData: Buffer.from(
          /* prettier-ignore */
          [
            '00000011',         /* chunk length 17 */
            '49444154',         /* chunk type IDAT */
            '7801010600f9ff',   /* zlib compression data part 1 */
            '000840',           /* scanline 1 */
            '0080f0',           /* scanline 2 */
            '031e01b9',         /* zlib CRC of uncompressed data */
            'b4342653',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
      }));
  });

  describe('given a 16-bit image buffer', () => {
    beforeEach(() => {
      impl.bitDepth = 16;
      impl.compressionLevel = zlib.constants.Z_NO_COMPRESSION;
      impl.height = 3;
      impl.image = Buffer.from('acdc12341234acdcacdc1234', 'hex');
      impl.margin = 1;
      impl.marginColor = { colorNumber: 0xf2f2 };
      impl.scale = 2;
      impl.width = 2;
    });

    it('(getPngBuffer) should build and return a valid png buffer', () =>
      validateAndCompareChunks(impl.getPngBuffer(), {
        chunkHeader: Buffer.from(
          /* prettier-ignore */
          [
            '0000000d',           /* chunk length 13 */
            '49484452',           /* chunk type IHDR */
            '0000000600000008',   /* image width, then height */
            '1000000000',         /* image bit depth, color type, compression, filter, and interlace methods */
            'af3d0da7',           /* chunk CRC */
          ].join(''),
          'hex',
        ),
        chunkData: Buffer.from(
          /* prettier-ignore */
          [
            '00000073',           /* chunk length 115 */
            '49444154',           /* chunk type IDAT */
            '780101680097ff',     /* zlib compression data part 1 */
            '00f2f2f2f2f2f2f2f2f2f2f2f2', /* scanline 1 */
            '00f2f2acdcacdc12341234f2f2', /* scanline 2 */
            '00f2f2acdcacdc12341234f2f2', /* scanline 3 */
            '00f2f212341234acdcacdcf2f2', /* scanline 4 */
            '00f2f212341234acdcacdcf2f2', /* scanline 5 */
            '00f2f2acdcacdc12341234f2f2', /* scanline 6 */
            '00f2f2acdcacdc12341234f2f2', /* scanline 7 */
            '00f2f2f2f2f2f2f2f2f2f2f2f2', /* scanline 8 */
            'a6ef4309',           /* zlib CRC of uncompressed data */
            'ac8abbcc',           /* chunk CRC */
          ].join(''),
          'hex',
        ),
      }));
  });

  describe('given a 16-bit image 2d array', () => {
    beforeEach(() => {
      /* deliberately not setting length and width so it can infer from the array */
      impl.bitDepth = 16;
      impl.compressionLevel = zlib.constants.Z_NO_COMPRESSION;
      impl.image = [
        [0xacdc, 0x1234],
        [0x1234, 0xacdc],
        [0xacdc, 0x1234],
      ];
      impl.margin = 1;
      impl.marginColor = { colorNumber: 0xf2f2 };
      impl.scale = 2;
    });

    it('(getPngBuffer) should build and return a valid png buffer', () =>
      validateAndCompareChunks(impl.getPngBuffer(), {
        chunkHeader: Buffer.from(
          /* prettier-ignore */
          [
            '0000000d',           /* chunk length 13 */
            '49484452',           /* chunk type IHDR */
            '0000000600000008',   /* image width, then height */
            '1000000000',         /* image bit depth, color type, compression, filter, and interlace methods */
            'af3d0da7',           /* chunk CRC */
          ].join(''),
          'hex',
        ),
        chunkData: Buffer.from(
          /* prettier-ignore */
          [
            '00000073',           /* chunk length 115 */
            '49444154',           /* chunk type IDAT */
            '780101680097ff',     /* zlib compression data part 1 */
            '00f2f2f2f2f2f2f2f2f2f2f2f2', /* scanline 1 */
            '00f2f2acdcacdc12341234f2f2', /* scanline 2 */
            '00f2f2acdcacdc12341234f2f2', /* scanline 3 */
            '00f2f212341234acdcacdcf2f2', /* scanline 4 */
            '00f2f212341234acdcacdcf2f2', /* scanline 5 */
            '00f2f2acdcacdc12341234f2f2', /* scanline 6 */
            '00f2f2acdcacdc12341234f2f2', /* scanline 7 */
            '00f2f2f2f2f2f2f2f2f2f2f2f2', /* scanline 8 */
            'a6ef4309',           /* zlib CRC of uncompressed data */
            'ac8abbcc',           /* chunk CRC */
          ].join(''),
          'hex',
        ),
      }));
  });

  describe('given a margin color number outside the range for the bit depth', () => {
    it('(fillData) should error', () =>
      [
        [8, -1],
        [8, 0x01ff],
        [16, -1],
        [16, 0x01ffff],
      ].forEach(([bitDepth, marginColorNumber]) => {
        impl.bitDepth = bitDepth;
        impl.image = [
          [0x40, 0x80],
          [0xc0, 0xf0],
        ];
        impl.margin = 1;
        impl.marginColor = {
          colorNumber: marginColorNumber,
        };

        const buffer: Buffer = Buffer.alloc(128);

        expect(() => (impl as any).fillData(buffer, 0)).to.throw();
      }));
  });

  describe('given an 8-bit 3x3 image 2d array', () => {
    beforeEach(() => {
      impl.compressionLevel = zlib.constants.Z_NO_COMPRESSION;
      impl.image = [
        [0xac, 0x12, 0xac],
        [0x12, 0xac, 0x12],
        [0xac, 0x12, 0xac],
      ];
      impl.margin = 1;
      impl.marginColor = { colorNumber: 0x83 };
    });

    it('(getPngBuffer) should build and return a valid png buffer', () =>
      validateAndCompareChunks(impl.getPngBuffer(), {
        chunkHeader: Buffer.from(
          /* prettier-ignore */
          [
            '0000000d',         /* chunk length 13 */
            '49484452',         /* chunk type IHDR */
            '0000000500000005', /* image width, then height */
            '0800000000',       /* image bit depth, color type, compression, filter, and interlace methods */
            'a8047939',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
        chunkData: Buffer.from(
          /* prettier-ignore */
          [
            '00000029',         /* chunk length 41 */
            '49444154',         /* chunk type IDAT */
            '7801011e00e1ff',   /* zlib compression data part 1 */
            '008383838383',     /* scanline 1 */
            '0083ac12ac83',     /* scanline 2 */
            '008312ac1283',     /* scanline 3 */
            '0083ac12ac83',     /* scanline 4 */
            '008383838383',     /* scanline 5 */
            'b18a0bd5',         /* zlib CRC of uncompressed data */
            '3f3d6b49',         /* chunk CRC */
          ].join(''),
          'hex',
        ),
      }));
  });

  describe('given a 16-bit 3x3 image 2d array', () => {
    beforeEach(() => {
      impl.bitDepth = 16;
      impl.compressionLevel = zlib.constants.Z_NO_COMPRESSION;
      impl.image = [
        [0xacdc, 0x1234, 0xacdc],
        [0x1234, 0xacdc, 0x1234],
        [0xacdc, 0x1234, 0xacdc],
      ];
      impl.margin = 1;
      impl.marginColor = { colorNumber: 0x8383 };
    });

    it('(getPngBuffer) should build and return a valid png buffer', () =>
      validateAndCompareChunks(impl.getPngBuffer(), {
        chunkHeader: Buffer.from(
          /* prettier-ignore */
          [
            '0000000d',               /* chunk length 13 */
            '49484452',               /* chunk type IHDR */
            '0000000500000005',       /* image width, then height */
            '1000000000',             /* image bit depth, color type, compression, filter, and interlace methods */
            'f894a57a',               /* chunk CRC */
          ].join(''),
          'hex',
        ),
        chunkData: Buffer.from(
          /* prettier-ignore */
          [
            '00000042',               /* chunk length 66 */
            '49444154',               /* chunk type IDAT */
            '7801013700c8ff',         /* zlib compression data part 1 */
            '0083838383838383838383', /* scanline 1 */
            '008383acdc1234acdc8383', /* scanline 2 */
            '0083831234acdc12348383', /* scanline 3 */
            '008383acdc1234acdc8383', /* scanline 4 */
            '0083838383838383838383', /* scanline 5 */
            'b2891921',               /* zlib CRC of uncompressed data */
            '18283d3f',               /* chunk CRC */
          ].join(''),
          'hex',
        ),
      }));

    describe('given some compression', () => {
      beforeEach(() => (impl.compressionLevel = 9));

      it('(getPngBuffer) should build and return a valid png buffer', () =>
        validateAndCompareChunks(impl.getPngBuffer(), {
          chunkHeader: Buffer.from(
            /* prettier-ignore */
            [
              '0000000d',               /* chunk length 13 */
              '49484452',               /* chunk type IHDR */
              '0000000500000005',       /* image width, then height */
              '1000000000',             /* image bit depth, color type, compression, filter, and interlace methods */
              'f894a57a',               /* chunk CRC */
            ].join(''),
            'hex',
          ),
          chunkData: Buffer.from(
            /* prettier-ignore */
            [
              '0000001e',               /* chunk length 30 */
              '49444154',               /* chunk type IDAT */
              '78da6368860386',         /* zlib compression data part 1 */
              /* note these compressed data chunks are arbitrary */
              'e6e63577844cd6dc013341', /* compressed data 1 */
              '0c21137451180000',       /* compressed data 2 */
              'b2891921',               /* zlib CRC of uncompressed data */
              '82268584',               /* chunk CRC */
            ].join(''),
            'hex',
          ),
        }));
    });
  });
});
