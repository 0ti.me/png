export function getRandomInt(max?: number): number {
  return Math.floor(Math.random() * max);
}

export function getRandomEntry<T>(array?: T[]): T {
  return array[getRandomInt(array.length)];
}
