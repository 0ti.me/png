import { expect } from '@0ti.me/ts-test-deps';

const GREY_BG = '\x1b[48;2;32;32;32m';
const RED_FG = '\x1b[38;2;255;0;0m';
const GREEN_FG = '\x1b[38;2;0;255;0m';
const RESET = '\x1b[0m';

const CHUNK_MAGIC_PNG = Buffer.from('89504e470d0a1a0a', 'hex');
const CHUNK_IEND = Buffer.from('0000000049454e44ae426082', 'hex');

export interface IValidateChunksParam {
  chunkMagic?: Buffer;
  chunkHeader: Buffer;
  chunkData: Buffer;
  chunkEnd?: Buffer;
}

export const createPartComparator =
  (name: string, sliceStart: number = 0, sliceEnd?: number) =>
  (
    bufferKey: string,
    actual: IValidateChunksParam,
    expected: IValidateChunksParam,
    finalRemarks: string = '',
  ): any =>
    expect(
      actual[bufferKey].slice(sliceStart, sliceEnd).toString('hex'),
      `${bufferKey} ${name} fields did not match\n${finalRemarks}`,
    ).to.equal(expected[bufferKey].slice(sliceStart, sliceEnd).toString('hex'));

const compareLength = createPartComparator('length', 0, 4);
const compareType = createPartComparator('type', 4, 8);
const compareData = createPartComparator('data', 8, -4);
const compareCrc32 = createPartComparator('crc32', -4);

export { compareCrc32, compareData, compareLength, compareType };

export function parseChunks(buffer: Buffer): IValidateChunksParam {
  return {
    chunkMagic: buffer.slice(0, 8),
    chunkHeader: buffer.slice(8, 33),
    chunkData: buffer.slice(33, buffer.length - 12),
    chunkEnd: buffer.slice(buffer.length - 12, buffer.length),
  };
}

export function compareChunkMagic(
  actual: IValidateChunksParam,
  expected: IValidateChunksParam,
  finalRemarks: string = '',
): void {
  expect(
    actual.chunkMagic.toString('hex'),
    `magic chunk \n${finalRemarks}`,
  ).to.equal(expected.chunkMagic.toString('hex'));
}

export function compareChunkHeader(
  actual: IValidateChunksParam,
  expected: IValidateChunksParam,
  finalRemarks: string = '',
): void {
  if (Buffer.compare(actual.chunkHeader, expected.chunkHeader) === 0) return;

  compareLength('chunkHeader', actual, expected);
  compareType('chunkHeader', actual, expected);
  compareData('chunkHeader', actual, expected);
  compareCrc32('chunkHeader', actual, expected);

  expect(
    actual.chunkHeader.toString('hex'),
    `header chunk\n${finalRemarks}`,
  ).to.equal(expected.chunkHeader.toString('hex'));
}

export function compareChunkData(
  actual: IValidateChunksParam,
  expected: IValidateChunksParam,
  finalRemarks: string = '',
): void {
  if (Buffer.compare(actual.chunkData, expected.chunkData) === 0) return;

  let sliceActual: string;
  let sliceExpected: string;

  /* switch between 0 and 1 based */
  const based = 0;
  const scanlineCount = actual.chunkHeader.readUInt32BE(12);
  const scanlineWidth = actual.chunkHeader.readUInt32BE(8);
  /* TODO: compensate for color and alpha with bitDepthFactor */
  const bitDepthFactor = actual.chunkHeader.readUInt8(16) / 8.0;
  const scanlineWidthBytes = 1 + Math.ceil(scanlineWidth * bitDepthFactor);
  const scanlineSets: string[][] = [];

  /* TODO this is all weird with compression */

  if (
    scanlineWidthBytes > 1 &&
    bitDepthFactor >= 0.125 &&
    bitDepthFactor <= 2 &&
    scanlineCount < 50000
  ) {
    let offset: number;

    const expChunkData = expected.chunkData.slice(0, -8);

    for (let i = 0; i < scanlineCount; ++i) {
      offset = 15 + i * scanlineWidthBytes;
      sliceActual = actual.chunkData
        .slice(offset, offset + scanlineWidthBytes)
        .toString('hex');
      sliceExpected =
        expChunkData /* slice from a subset so if it's empty it'll just be blank */
          .slice(offset, offset + scanlineWidthBytes)
          .toString('hex');

      scanlineSets.push([sliceActual, sliceExpected]);
    }
  } else {
    const fishyDataObj = {
      bf: bitDepthFactor,
      sb: scanlineWidthBytes,
      sc: scanlineCount,
      sw: scanlineWidth,
    };

    const fishyData = `\nfishy data: ${JSON.stringify(fishyDataObj)}`;
    const fullHeader = `\nactual header buffer: 0x${actual.chunkHeader.toString(
      'hex',
    )}`;

    expect(
      actual.chunkData.toString('hex'),
      `something fishy with this data, may be malformed header\n${finalRemarks}${fishyData}${fullHeader}`,
    ).to.equal(expected.chunkData.toString('hex'));
  }

  /* finally check if the IDAT chunk data except the zlib bytes is the same */
  sliceActual = actual.chunkData.slice(15, -8).toString('hex');
  sliceExpected = actual.chunkData.slice(15, -8).toString('hex');

  const lnAct = actual.chunkData.slice(0, 4).toString('hex');
  const lnExp = expected.chunkData.slice(0, 4).toString('hex');
  const tyAct = actual.chunkData.slice(4, 8).toString('hex');
  const tyExp = expected.chunkData.slice(4, 8).toString('hex');
  const z1Act = actual.chunkData.slice(8, 15).toString('hex');
  const z1Exp = expected.chunkData.slice(8, 15).toString('hex');
  const z2Act = actual.chunkData.slice(-8, -4).toString('hex');
  const z2Exp = expected.chunkData.slice(-8, -4).toString('hex');
  const crcAc = actual.chunkData.slice(-4).toString('hex');
  const crcEx = expected.chunkData.slice(-4).toString('hex');

  const usefulMessages = [];
  let welcome: boolean = false;

  scanlineSets.forEach(([a, e], i) => {
    if (a !== e) {
      if (welcome === false) {
        welcome = true;
        usefulMessages.push(
          `\ninterleaved scanlines for comparing ${GREY_BG}${GREEN_FG}+ expected ${RED_FG}- actual${RESET}`,
        );
      }

      usefulMessages.push(
        `${RESET}\nscanline[${i + based}]   ${RED_FG}-${GREY_BG}${a}${RESET}`,
      );
      usefulMessages.push(
        `${RESET}\nscanline[${i + based}]   ${GREEN_FG}+${GREY_BG}${e}${RESET}`,
      );
    }
  });

  if (lnAct !== lnExp) {
    usefulMessages.push(
      `${RESET}\nlength      ${RED_FG}-${GREY_BG}${lnAct}${RESET}`,
    );
    usefulMessages.push(
      `${RESET}\nlength      ${GREEN_FG}+${GREY_BG}${lnExp}${RESET}`,
    );
  }

  if (tyAct !== tyExp) {
    usefulMessages.push(
      `${RESET}\ntype        ${RED_FG}-${GREY_BG}${tyAct}${RESET}`,
    );
    usefulMessages.push(
      `${RESET}\ntype        ${GREEN_FG}+${GREY_BG}${tyExp}${RESET}`,
    );
  }

  if (z1Act !== z1Exp) {
    usefulMessages.push(
      `${RESET}\nzlib part 1 ${RED_FG}-${GREY_BG}${z1Act}${RESET}`,
    );
    usefulMessages.push(
      `${RESET}\nzlib part 1 ${GREEN_FG}+${GREY_BG}${z1Exp}${RESET}`,
    );
  }

  if (z2Act !== z2Exp) {
    usefulMessages.push(
      `${RESET}\nzlib crc    ${RED_FG}-${GREY_BG}${z2Act}${RESET}`,
    );
    usefulMessages.push(
      `${RESET}\nzlib crc    ${GREEN_FG}+${GREY_BG}${z2Exp}${RESET}`,
    );
  }

  if (crcAc !== crcEx) {
    usefulMessages.push(
      `${RESET}\ncrc         ${RED_FG}-${GREY_BG}${crcAc}${RESET}`,
    );
    usefulMessages.push(
      `${RESET}\ncrc         ${GREEN_FG}+${GREY_BG}${crcEx}${RESET}`,
    );
  }

  const usefulMessage = usefulMessages.join('');

  sliceActual = actual.chunkData.slice(8, 15).toString('hex');
  sliceExpected = expected.chunkData.slice(8, 15).toString('hex');

  scanlineSets.forEach(([a, e], i) =>
    expect(
      a,
      `first scanline error found in scanline [${based}-based] ${
        i + based
      }${finalRemarks}${usefulMessage}`,
    ).to.equal(e),
  );

  expect(
    sliceActual,
    `chunkData zlib part 1 did not match\n${finalRemarks}${usefulMessage}`,
  ).to.equal(sliceExpected);

  expect(
    actual.chunkData.slice(-8, -4).toString('hex'),
    `chunkData zlib crc of uncompressed data did not match\n${finalRemarks}${usefulMessage}`,
  ).to.equal(expected.chunkData.slice(-8, -4).toString('hex'));

  compareLength(
    'chunkData',
    actual,
    expected,
    `${finalRemarks}${usefulMessage}`,
  );

  compareType(
    'chunkData',
    actual,
    expected,
    `\n${finalRemarks}${usefulMessage}`,
  );

  compareCrc32(
    'chunkData',
    actual,
    expected,
    `\n${finalRemarks}${usefulMessage}`,
  );

  expect(
    actual.chunkData.toString('hex'),
    `data chunk, see scanlines above \n${finalRemarks}${usefulMessage}`,
  ).to.equal(expected.chunkData.toString('hex'));
}

export function compareChunkEnd(
  actual: IValidateChunksParam,
  expected: IValidateChunksParam,
  finalRemarks: string = '',
): void {
  if (Buffer.compare(actual.chunkEnd, expected.chunkEnd) === 0) return;

  compareLength('chunkEnd', actual, expected);
  compareType('chunkEnd', actual, expected);
  compareData('chunkEnd', actual, expected);
  compareCrc32('chunkEnd', actual, expected);

  /* TODO: compare chunk components individually */
  expect(
    actual.chunkEnd.toString('hex'),
    `end chunk\n${finalRemarks}`,
  ).to.equal(expected.chunkEnd.toString('hex'));
}

export function validateAndCompareChunks(
  chunkActual: IValidateChunksParam | Buffer,
  chunkExpected: IValidateChunksParam | Buffer,
): void {
  const expected: IValidateChunksParam =
    chunkExpected instanceof Buffer
      ? parseChunks(chunkExpected)
      : Object.assign(
          {
            /**
             * In almost all cases, we won't want to have to specify magic PNG or chunk end,
             * so we'll default them for the expected value
             */
            chunkMagic: CHUNK_MAGIC_PNG,
            chunkEnd: CHUNK_IEND,
          },
          chunkExpected,
        );
  const actual: IValidateChunksParam =
    chunkActual instanceof Buffer ? parseChunks(chunkActual) : chunkActual;

  const messages: string[] = [];

  const addMessages = (
    actChunk: Buffer,
    expChunk: Buffer,
    prefix: string,
  ): void => {
    messages.push(
      `${RESET}${prefix}: (${GREY_BG}${RED_FG}actual${RESET} !== ${GREY_BG}${GREEN_FG}expected${RESET})`,
    );
    messages.push(`\n${GREY_BG}${RED_FG}${actChunk.toString('hex')}${RESET}`);
    messages.push(`\n${GREY_BG}${GREEN_FG}${expChunk.toString('hex')}${RESET}`);
  };

  /* log when differences exist ahead of expectations */
  if (Buffer.compare(actual.chunkMagic, expected.chunkMagic) !== 0) {
    addMessages(actual.chunkMagic, expected.chunkMagic, 'magic png header');
  }

  if (Buffer.compare(actual.chunkHeader, expected.chunkHeader) !== 0) {
    addMessages(actual.chunkHeader, expected.chunkHeader, 'chunk IHDR');
  }

  if (Buffer.compare(actual.chunkData, expected.chunkData) !== 0) {
    addMessages(actual.chunkData, expected.chunkData, 'chunk IDAT');
  }

  if (Buffer.compare(actual.chunkEnd, expected.chunkEnd) !== 0) {
    addMessages(actual.chunkData, expected.chunkData, 'chunk IEND');
  }

  /* compare from easiest chunks to least easy chunks */
  compareChunkMagic(actual, expected, messages.join(''));
  compareChunkEnd(actual, expected, messages.join(''));
  compareChunkHeader(actual, expected, messages.join(''));
  compareChunkData(actual, expected, messages.join(''));
}
