import { BitArray } from '../submodules/bitwise/src/bit-array';
import { CONSTANTS } from './png-implementation';
import { GrayscalePngImplementation } from './grayscale-png-implementation';
import * as zlib from 'zlib';

const { IDAT } = CONSTANTS;

const MASKS = [
  null /* no support for bitDepth 0 */,
  0x01 /* bit depth 1 */,
  0x03 /* bit depth 2 */,
  null /* no support for bitDepth 3 */,
  0x0f /* bit depth 4 */,
];

export class BitGrayscalePngImplementation extends GrayscalePngImplementation {
  protected defaultBitDepth: number = 1;
  protected validBitDepths: number[] = [1, 2, 4];

  public get colorType(): number {
    /* 0 means grayscale */
    return 0x00;
  }

  public get filterMethod(): number {
    /* TODO support filter methods */
    return 0x00;
  }

  public get interlaceMethod(): number {
    /* TODO support interlace methods */
    return 0x00;
  }

  protected getValue(col: number, row: number, mask?: number): number {
    if (mask === undefined) mask = MASKS[this.bitDepth];

    const val: number = this.getSourceValue(col, row);

    return val & mask;
  }

  protected fillData(
    buffer: Buffer,
    offset: number,
  ): BitGrayscalePngImplementation {
    const bitDepth = this.bitDepth;
    const mask = MASKS[bitDepth];

    const uncompressedDataChunkBufferLength =
      this.uncompressedDataChunkBufferLength;

    const dataBuffer = buffer.slice(
      offset,
      offset + uncompressedDataChunkBufferLength,
    );

    const bitArray: BitArray = new BitArray({
      initialBuffer: dataBuffer,
    });

    /* push the uncompressed length and a chunk type of zero to just reserve that space */
    bitArray.addUInt32(uncompressedDataChunkBufferLength - 12);
    bitArray.addUInt32(0x00);
    /* copy the IDAT portion over the zeros */
    IDAT.copy(dataBuffer, 4);

    const marginColorNumber = this.marginColor.colorNumber & mask;

    let bitWriteCount: number;
    let i: number;
    let j: number;
    let k1: number;
    let k2: number;
    let marginUInt32: number = 0;
    let sideMarginUInt32Count: number = 0;
    let sideMarginVal: number;
    let val: number;

    /* TODO implement filtering */
    const filterByte: number = 0x00;
    const scaledWidthPlusMargin: number = this.scaledWidthPlusMargin;

    const marginBits: number = this.margin * bitDepth;
    let marginOnlyRowBitArray: BitArray;
    let marginOnlyRowBuffer: Buffer;
    let remainingMarginBits: number = marginBits;

    if (this.margin > 0) {
      /* set up margin values */
      for (i = 0; i < 32; i += bitDepth) {
        marginUInt32 <<= bitDepth;
        marginUInt32 += marginColorNumber;
      }

      marginUInt32 >>>= 0;

      if (marginBits >= 32) {
        sideMarginUInt32Count = Math.floor(marginBits / 32.0);

        remainingMarginBits = marginBits % 32;
      }

      sideMarginVal = 0;

      for (i = 0; i < remainingMarginBits; i += bitDepth) {
        sideMarginVal <<= bitDepth;
        sideMarginVal += marginColorNumber;
      }

      marginOnlyRowBitArray = new BitArray({
        initialBuffer: Buffer.alloc(
          1 + Math.ceil(scaledWidthPlusMargin * bitDepth),
        ),
      });

      marginOnlyRowBitArray.addUInt8(filterByte);

      for (i = 0; i < scaledWidthPlusMargin; ++i) {
        marginOnlyRowBitArray.addLSBits(marginColorNumber, bitDepth);
      }

      marginOnlyRowBuffer = marginOnlyRowBitArray.buffer;

      /* add top margin scanlines */
      for (i = 0; i < this.margin; ++i) {
        bitArray.addBuffer(marginOnlyRowBuffer);
      }
    }

    /* TODO further optimization here */
    for (i = 0; i < this.height; ++i) {
      for (k1 = 0; k1 < this.scale; ++k1) {
        bitWriteCount = 0;
        val = 0;

        /* add scanline filter byte */
        bitArray.addUInt8(filterByte);

        /* add full 32-bit margins */
        for (j = 0; j < sideMarginUInt32Count; ++j) {
          bitArray.addUInt32(marginUInt32);
        }

        /* add less than 32-bit margins */
        if (remainingMarginBits > 0) {
          bitArray.addLSBits(sideMarginVal, remainingMarginBits);
        }

        for (j = 0; j < this.width; ++j) {
          for (k2 = 0; k2 < this.scale; ++k2) {
            val = val << bitDepth;
            val += this.getValue(j, i, mask);

            bitWriteCount += bitDepth;

            if (bitWriteCount === 32) {
              bitArray.addUInt32(val);
              bitWriteCount = 0;
            }
          }
        }

        if (bitWriteCount > 0) {
          bitArray.addLSBits(val >>> 0, bitWriteCount);
        }

        /* add full 32-bit margins */
        for (j = 0; j < sideMarginUInt32Count; ++j) {
          bitArray.addUInt32(marginUInt32);
        }

        /* add less than 32-bit margins */
        if (remainingMarginBits > 0) {
          bitArray.addLSBits(sideMarginVal, remainingMarginBits);
        }

        /* each scanline should be terminated with zeros to make up a byte if need be */
        bitArray.padLengthToNextByte();
      }
    }

    if (this.margin > 0) {
      /* add bottom margin scanlines */
      for (i = 0; i < this.margin; ++i) {
        bitArray.addBuffer(marginOnlyRowBuffer);
      }
    }

    const compressedData = zlib.deflateSync(
      /**
       * do compression on just the data portion of the buffer
       * Starting at 8 to skip length and chunk type
       * Stopping at -15 because no compression data has been written yet and to skip the CRC
       */
      dataBuffer.slice(8, dataBuffer.length - 15),
      this.deflateOptions,
    );

    /**
     * copy the compressed data over the buffer
     *
     * Note that even though some characters might be left over from the uncompressed section,
     * it will either be overwritten by the end chunk when written or it will be excluded by
     * a buffer slice at the end.
     */
    compressedData.copy(dataBuffer, 8);

    /* write the compressed length */
    if (compressedData.length !== dataBuffer.length - 12) {
      dataBuffer.writeUInt32BE(compressedData.length);
    }

    /* write the CRC */
    this.fillCRC(dataBuffer, compressedData.length + 8, 4);

    /* inform the total number of bytes written for the entire data chunk */
    this.writtenDataChunkBufferLength = compressedData.length + 12;

    return this;
  }
}
