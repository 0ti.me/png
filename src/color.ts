export interface IColor {
  colorNumber: number;
}

export interface IBasicColor {
  red?: number;
  green?: number;
  blue?: number;
}

export interface IAlphaColor extends IBasicColor {
  alpha?: number;
}

/* TODO these naive implementations need to consider 16-bit depth pixels, making 48-bit and 64-bit integers below
export class Color implements IColor, IBasicColor {
  red: number;
  green: number;
  blue: number;

  get colorNumber(): number {
    return (this.red << (16 + this.green)) << (8 + this.blue);
  }

  constructor(basicColor: IBasicColor) {
    this.red = basicColor.red ?? 0;
    this.green = basicColor.green ?? 0;
    this.blue = basicColor.blue ?? 0;
  }
}

export class AlphaColor extends Color implements IAlphaColor {
  red: number;
  green: number;
  blue: number;
  alpha: number;

  get colorNumber(): number {
    return (super.colorNumber << (8 + this.alpha)) >>> 0;
  }

  constructor(alphaColor: IAlphaColor) {
    super(alphaColor);

    this.alpha = alphaColor.alpha ?? 0;
  }
}
*/

export class GrayscaleColor implements IColor {
  brightness: number;

  get colorNumber(): number {
    return this.brightness;
  }

  constructor(brightness: number) {
    this.brightness = brightness;
  }
}
