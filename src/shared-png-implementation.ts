import { crc32 } from '../submodules/crc32/src/index';
import { IColor } from './color';
import { CONSTANTS, IPngImplementation } from './png-implementation';
import * as zlib from 'zlib';
import { ZlibOptions } from 'zlib';

const { IEND, IHDR, PNG_MAGIC } = CONSTANTS;

/**
 * Implements the core shared functionality across all png implementations
 * Key reference links:
 * - http://www.libpng.org/pub/png/spec/1.2/PNG-Structure.html#Chunk-layout
 * - http://www.libpng.org/pub/png/spec/1.2/PNG-Chunks.html
 */
export abstract class SharedPngImplementation implements IPngImplementation {
  protected abstract get bitDepth(): number;
  /* bytes per complete pixel (rounded up) */
  protected abstract get bpp(): number;
  protected abstract get scale(): number;

  private mCompressionLevel: number;
  private mMargin: number;
  private mScaleMargin: boolean;

  constructor() {
    this.mScaleMargin = true; /* default to true */
  }

  protected get bitDepthFactor(): number {
    /* TODO: this might actually need to be abstract and defined differently for grayscale vs color */
    return this.bitDepth / 8.0;
  }

  public get compressionLevel(): number {
    return this.mCompressionLevel === undefined
      ? zlib.constants.Z_DEFAULT_COMPRESSION
      : this.mCompressionLevel;
  }

  /**
   * Sets the compression level according to the requested compression level with sane defaults and assumptions
   */
  public set compressionLevel(pCompressionLevel: number) {
    if (pCompressionLevel < -1 || pCompressionLevel > 9) {
      throw new Error(
        `unknown compression level ${pCompressionLevel}, defaulting to no compression`,
      );
    } else {
      this.mCompressionLevel = pCompressionLevel;
    }
  }

  protected get deflateOptions(): ZlibOptions {
    return {
      level: this.compressionLevel,
    };
  }

  /**
   * Margin does not get scaled with everything else
   */
  public get margin(): number {
    return (this.mMargin ?? 0) * this.marginScale;
  }

  /**
   * Margin does not get scaled with everything else
   */
  public set margin(pMargin: number) {
    this.mMargin = pMargin;
  }

  public get marginScale(): number {
    return this.mScaleMargin ? this.scale : 1;
  }

  public set scaleMargin(pScaleMargin: boolean) {
    this.mScaleMargin = pScaleMargin;
  }

  abstract get marginColor(): IColor;
  abstract set marginColor(color: IColor);

  /* png magic buffer properties */
  protected get pngMagicChunkBufferLength(): number {
    return PNG_MAGIC.length;
  }

  protected get pngMagicChunkBufferStartOffset(): number {
    return 0;
  }

  /* header buffer properties */
  protected get headerChunkBufferStartOffset(): number {
    return this.pngMagicChunkBufferStartOffset + this.pngMagicChunkBufferLength;
  }

  protected get headerChunkBufferLength(): number {
    /**
     * Chunk layout Length 4 bytes
     * Chunk layout Type   4 bytes
     * Width:              4 bytes
     * Height:             4 bytes
     * Bit depth:          1 byte
     * Color type:         1 byte
     * Compression method: 1 byte
     * Filter method:      1 byte
     * Interlace method:   1 byte
     * Chunk layout CRC    4 bytes
     */
    return 25;
  }

  protected get scaledHeight(): number {
    return this.height * this.scale;
  }

  protected get scaledHeightPlusMargin(): number {
    return this.scaledHeight + 2 * this.margin;
  }

  protected get scaledWidth(): number {
    return this.width * this.scale;
  }

  protected get scaledWidthPlusMargin(): number {
    return this.scaledWidth + 2 * this.margin;
  }

  protected abstract get height(): number;
  protected abstract get width(): number;
  protected abstract get colorType(): number;

  /* overloadable in case this ever changes for some/all implementations */
  protected get compressionMethod(): number {
    return 0x00;
  }

  public get filterMethod(): number {
    /* TODO support filter methods */
    return 0x00;
  }

  public get interlaceMethod(): number {
    /* TODO support interlace methods */
    return 0x00;
  }

  /* data buffer properties */
  protected get dataChunkBufferStartOffset(): number {
    return 33;
  }

  /**
   * This length field only really provides any value for uncompressed PNG outputs or
   * for initialization of a pre-compressed buffer
   */
  protected get uncompressedDataChunkBufferLength(): number {
    /* TODO how can I make this calculation happen less frequently?
     * - Set this whenever a field is updated?
     * - Set it once at some crucial moment?
     */

    /* TODO verify that these calculations are right in each case */
    const pixelBytesPerRow: number = Math.ceil(
      /* TODO: how does this change with color information? */
      this.scaledWidthPlusMargin * this.bitDepthFactor,
    );

    const totalHeight: number = this.scaledHeightPlusMargin;
    const scaledArea: number = totalHeight * pixelBytesPerRow;

    /* TODO: does this calculation change if a different (not 0) filter method is used? */

    /**
     * Scaled area for the entire image space
     * Chunk layout Length 4 bytes
     * Chunk layout Type   4 bytes
     * zlib part 1         7 bytes
     * filter bytes        h bytes
     * data bytes          x bytes (see scaledArea calculation)
     * zlib part 2         4 bytes
     * Chunk layout CRC    4 bytes
     * aka x + h + 23
     */
    return scaledArea + totalHeight + 23;
  }

  protected abstract get writtenDataChunkBufferLength(): number;

  protected get endChunkBufferLength(): number {
    /**
     * Chunk layout Length 4 bytes
     * Chunk layout Type   4 bytes
     * Chunk layout CRC    4 bytes
     */
    return 12;
  }

  protected fillMagicPng(
    buffer: Buffer,
    offset: number,
  ): SharedPngImplementation {
    PNG_MAGIC.copy(buffer, offset);

    return this;
  }

  protected fillHeader(
    buffer: Buffer,
    offset: number,
  ): SharedPngImplementation {
    offset = this.headerChunkBufferStartOffset;

    /* length excludes chunk length, chunk type, and crc */
    buffer.writeUInt32BE(13, offset);
    IHDR.copy(buffer, offset + 4);
    buffer.writeUInt32BE(this.scaledWidthPlusMargin, offset + 8);
    buffer.writeUInt32BE(this.scaledHeightPlusMargin, offset + 12);
    buffer.writeUInt8(this.bitDepth, offset + 16);
    buffer.writeUInt32BE(
      (((this.colorType << (24 + this.compressionMethod)) <<
        (16 + this.filterMethod)) <<
        (8 + this.interlaceMethod)) >>>
        0,
      offset + 17,
    );
    this.fillCRC(buffer, offset + 21, offset + 4);

    return this;
  }

  protected abstract fillData(
    buffer: Buffer,
    offset: number,
  ): SharedPngImplementation;

  protected fillEnd(buffer: Buffer, offset: number): SharedPngImplementation {
    buffer.writeUInt32BE(0x00, offset);
    IEND.copy(buffer, offset + 4);
    buffer.writeUInt32BE(0xae426082 >>> 0, offset + 8);

    return this;
  }

  protected fillCRC(
    buffer: Buffer,
    writeOffset: number,
    readOffset: number,
    endReadOffset: number = writeOffset,
  ): SharedPngImplementation {
    buffer.writeUInt32BE(
      crc32(buffer.slice(readOffset, endReadOffset)),
      writeOffset,
    );

    return this;
  }

  public getPngBuffer(): Buffer {
    const sumUncompressedLengths: number =
      this.pngMagicChunkBufferLength +
      this.headerChunkBufferLength +
      this.uncompressedDataChunkBufferLength +
      this.endChunkBufferLength;

    const buffer: Buffer = Buffer.alloc(sumUncompressedLengths);

    this.fillMagicPng(buffer, this.pngMagicChunkBufferStartOffset)
      .fillHeader(buffer, this.headerChunkBufferStartOffset)
      .fillData(buffer, this.dataChunkBufferStartOffset);

    const endOffset =
      this.dataChunkBufferStartOffset + this.writtenDataChunkBufferLength;

    this.fillEnd(buffer, endOffset);

    return buffer.slice(0, endOffset + this.endChunkBufferLength);
  }
}
