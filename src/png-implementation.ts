export const CONSTANTS = {
  IHDR: Buffer.from('IHDR', 'utf8'),
  IDAT: Buffer.from('IDAT', 'utf8'),
  IEND: Buffer.from('IEND', 'utf8'),
  PNG_MAGIC: Buffer.from('89504e470d0a1a0a', 'hex'),
  STATIC_END_CHUNK: Buffer.from('0000000049454e44ae426082', 'hex'),
};

/**
 * an underlying png implementation which will be used by a given configuration,
 * allowing optimizations for various specific PNG configurations.
 *
 * For example, 1-bit grayscale PNGs need different optimizations relative to 8-bit grayscale PNGs
 */
export interface IPngImplementation {
  /**
   * Computes and produces a PNG buffer (raw bytes) ready for writing to file
   *
   * @returns a completed PNG buffer (raw bytes) ready for writing to file
   */
  getPngBuffer(): Buffer;
}

/**
 * The exposed interface for producing PNG buffers
 *
 * Should probably just select an appropriate IPngImplementation and pass getPngBuffer calls to that implementation
 */
export interface IPngBuffer extends IPngImplementation {
  /**
   * Loads a 2 dimensional array of rows of pixel data, example follows
   * - 2x2 with coordinates instead of pixels: [[(0,0), (0,1)], [(1,0), (1,1)]]
   *
   * @param imageArray: the two dimensional array of pixel data
   *
   * @returns IPngBuffer the writer for chaining
   */
  loadImage2dArray(imageArray: (number[] | Uint8Array | Buffer)[]): IPngBuffer;

  /**
   * Loads a 1 dimensional array of pixel data, with a width to define row cutoffs (data left->right then top->bottom)
   * - 2x2 with coordinates instead of pixels: [(0,0), (0,1), (1,0), (1,1)]
   *
   * @param imageArray: the one dimensional array of pixel data
   * @param width: the number of pixels per row
   *
   * @returns IPngBuffer the writer for chaining
   */
  loadImageArray(
    imageArray: number[] | Uint8Array | Buffer,
    width: number,
  ): IPngBuffer;

  /**
   * Sets the bit depth for the output
   *
   * For more detail, see the bit depth information here: http://www.libpng.org/pub/png/spec/1.2/PNG-Chunks.html#C.IHDR
   *
   * @param bitDepth: the value for the bit depth
   *
   * @returns IPngBuffer the writer for chaining
   */
  setBitDepth(bitDepth: number): IPngBuffer;

  /**
   * Sets the compression level for the output
   *
   * For more detail, see the level argument here: https://nodejs.org/api/zlib.html#zlib_class_options
   *
   * @param compressionLevel: the value for the compression level
   *
   * @returns IPngBuffer the writer for chaining
   */
  setCompressionLevel(compressionLevel: number): IPngBuffer;

  /**
   * Sets the margin and the value for the output.
   *
   * @param margin: the positive integer count of pixels of margin data desired in the output
   * @param value: the pixel data value for the margin
   *
   * @returns IPngBuffer the writer for chaining
   */
  setMargin(margin: number, value: number): IPngBuffer;

  /**
   * Sets a scalar to use to grow the imageArray to take up more space, it grows in both x and y coordinates
   *
   * For example, a scale of 3 will cause each pixel from your imageArray to take up 9 (3x3) pixels in the output
   *
   * @param scale: The whole positive integer scale factor
   *
   * @returns IPngBuffer the writer for chaining
   */
  setScale(scale: number): IPngBuffer;
}
