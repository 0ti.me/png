import { ByteArray } from '../submodules/bitwise/src/byte-array';
import { CONSTANTS } from './png-implementation';
import { GrayscalePngImplementation } from './grayscale-png-implementation';
import * as zlib from 'zlib';

const { IDAT } = CONSTANTS;

export class ByteGrayscalePngImplementation extends GrayscalePngImplementation {
  protected defaultBitDepth: number = 8;
  protected validBitDepths: number[] = [8, 16];

  protected getValue(col: number, row: number): number {
    return this.getSourceValue(col, row);
  }

  protected fillData(
    buffer: Buffer,
    offset: number,
  ): ByteGrayscalePngImplementation {
    const uncompressedDataChunkBufferLength =
      this.uncompressedDataChunkBufferLength;

    const dataBuffer = buffer.slice(
      offset,
      offset + uncompressedDataChunkBufferLength,
    );

    const byteArray: ByteArray = new ByteArray({
      initialBuffer: dataBuffer,
    });

    /* push the uncompressed length and a chunk type of zero to just reserve that space */
    byteArray.addUInt32(uncompressedDataChunkBufferLength - 12);
    byteArray.addUInt32(0x00);
    /* copy the IDAT portion over the zeros */
    IDAT.copy(dataBuffer, 4);

    const marginColorNumber = this.marginColor.colorNumber;

    if (
      this.bitDepth === 8 &&
      (marginColorNumber > 255 || marginColorNumber < 0)
    ) {
      throw new Error(
        `invalid margin color [${marginColorNumber.toString(
          16,
        )}] for bit depth [${this.bitDepth}]`,
      );
    } else if (
      this.bitDepth === 16 &&
      (marginColorNumber > 65535 || marginColorNumber < 0)
    ) {
      throw new Error(
        `invalid margin color [${marginColorNumber.toString(
          16,
        )}] for bit depth [${this.bitDepth}]`,
      );
    }

    let i: number;
    let j: number;
    let k1: number;
    let k2: number;
    let marginUInt32: number;

    const byteDepth = this.bitDepth / 8;
    /* TODO implement filtering */
    const filterByte: number = 0x00;
    const pixelsPer32Bits: number = 32 / this.bitDepth;
    const scaledWidthPlusMargin = this.scaledWidthPlusMargin;

    if (this.margin > 0) {
      /* add top margin scanlines */
      marginUInt32 =
        (this.bitDepth === 8
          ? (marginColorNumber << 24) +
            (marginColorNumber << 16) +
            (marginColorNumber << 8) +
            marginColorNumber
          : (marginColorNumber << 16) + marginColorNumber) >>> 0;

      for (i = 0; i < this.margin; ++i) {
        /* add scanline filter byte */
        byteArray.addUInt8(filterByte);

        for (
          j = 0;
          j + pixelsPer32Bits <= scaledWidthPlusMargin;
          j += pixelsPer32Bits
        ) {
          byteArray.addUInt32(marginUInt32);
        }

        for (; j < scaledWidthPlusMargin; j += byteDepth) {
          if (this.bitDepth === 8) {
            byteArray.addUInt8(marginColorNumber);
          } else {
            byteArray.addUInt16(marginColorNumber);
          }
        }
      }
    }

    /* TODO (probably) improve efficiency by copying more than one pixel at a time */
    for (i = 0; i < this.height; ++i) {
      for (k1 = 0; k1 < this.scale; ++k1) {
        byteArray.addUInt8(filterByte);

        if (this.margin > 0) {
          for (j = 0; j < this.margin; ++j) {
            if (this.bitDepth === 8) {
              byteArray.addUInt8(marginColorNumber);
            } else {
              byteArray.addUInt16(marginColorNumber);
            }
          }
        }

        for (j = 0; j < this.width; ++j) {
          for (k2 = 0; k2 < this.scale; ++k2) {
            if (this.bitDepth === 8) {
              byteArray.addUInt8(this.getValue(j, i));
            } else {
              byteArray.addUInt16(this.getValue(j, i));
            }
          }
        }

        if (this.margin > 0) {
          for (j = 0; j < this.margin; ++j) {
            if (this.bitDepth === 8) {
              byteArray.addUInt8(marginColorNumber);
            } else {
              byteArray.addUInt16(marginColorNumber);
            }
          }
        }
      }
    }

    if (this.margin > 0) {
      /* add bottom margin scanlines */
      marginUInt32 =
        (this.bitDepth === 8
          ? (marginColorNumber << 24) +
            (marginColorNumber << 16) +
            (marginColorNumber << 8) +
            marginColorNumber
          : (marginColorNumber << 16) + marginColorNumber) >>> 0;

      for (i = 0; i < this.margin; ++i) {
        /* add scanline filter byte */
        byteArray.addUInt8(filterByte);

        for (
          j = 0;
          j + pixelsPer32Bits <= scaledWidthPlusMargin;
          j += pixelsPer32Bits
        ) {
          byteArray.addUInt32(marginUInt32);
        }

        for (; j < scaledWidthPlusMargin; j += byteDepth) {
          if (this.bitDepth === 8) {
            byteArray.addUInt8(marginColorNumber);
          } else {
            byteArray.addUInt16(marginColorNumber);
          }
        }
      }
    }

    const compressedData = zlib.deflateSync(
      /**
       * do compression on just the data portion of the buffer
       * Starting at 8 to skip length and chunk type
       * Stopping at -15 because no compression data has been written yet and to skip the CRC
       */
      dataBuffer.slice(8, dataBuffer.length - 15),
      this.deflateOptions,
    );

    /**
     * copy the compressed data over the buffer
     *
     * Note that even though some characters might be left over from the uncompressed section,
     * it will either be overwritten by the end chunk when written or it will be excluded by
     * a buffer slice at the end.
     */
    compressedData.copy(dataBuffer, 8);

    /* write the compressed length */
    if (compressedData.length !== dataBuffer.length - 12) {
      dataBuffer.writeUInt32BE(compressedData.length);
    }

    /* write the CRC */
    this.fillCRC(dataBuffer, compressedData.length + 8, 4);

    /* inform the total number of bytes written for the entire data chunk */
    this.writtenDataChunkBufferLength = compressedData.length + 12;

    return this;
  }
}
