import { GrayscaleColor, IColor } from './color';
import { SharedPngImplementation } from './shared-png-implementation';

export abstract class GrayscalePngImplementation extends SharedPngImplementation {
  private mBitDepth: number;
  private mHeight: number;
  private mImage: Buffer | number[][];
  private mMarginColor: IColor;
  private mScale: number;
  private mWidth: number;
  private mWrittenDataChunkBufferLength: number;

  protected abstract get defaultBitDepth(): number;
  protected abstract get validBitDepths(): number[];

  /* bytes per complete pixel (rounded up) */
  protected get bpp(): number {
    return Math.ceil(this.bitDepth / 8.0);
  }

  public set bitDepth(pBitDepth: number) {
    if (!this.validBitDepths.includes(pBitDepth)) {
      throw new Error(
        `invalid bit depth for grayscale implementation ${pBitDepth}`,
      );
    }

    this.mBitDepth = pBitDepth;
  }

  public get bitDepth(): number {
    return this.mBitDepth ?? this.defaultBitDepth;
  }

  public get colorType(): number {
    /* 0 means grayscale */
    return 0x00;
  }

  public get height(): number {
    return this.mHeight;
  }

  public set height(pHeight: number) {
    this.mHeight = pHeight;
  }

  public set image(input: Buffer | number[][]) {
    this.mImage = input;

    /* sane initializations if not initialized */
    if (!(input instanceof Buffer)) {
      this.height ??= input.length;
      this.width ??= input[0].length;
    }
  }

  public get image(): Buffer | number[][] {
    return this.mImage;
  }

  public get marginColor(): IColor {
    /* default to white margin */
    return this.mMarginColor ?? new GrayscaleColor(0);
  }

  public set marginColor(pMarginColor: IColor) {
    this.mMarginColor = pMarginColor;
  }

  public get scale(): number {
    return this.mScale ?? 1;
  }

  public set scale(pScale: number) {
    this.mScale = pScale;
  }

  public get width(): number {
    return this.mWidth;
  }

  public set width(pWidth: number) {
    this.mWidth = pWidth;
  }

  protected get writtenDataChunkBufferLength(): number {
    if (this.mWrittenDataChunkBufferLength === undefined) {
      throw new Error('have not yet initialized written data chunk length');
    }

    return this.mWrittenDataChunkBufferLength;
  }

  protected set writtenDataChunkBufferLength(
    pWrittenDataChunkBufferLength: number,
  ) {
    this.mWrittenDataChunkBufferLength = pWrittenDataChunkBufferLength;
  }

  protected getSourceValue(col: number, row: number): number {
    if (col < 0) col = this.width + col;
    if (row < 0) row = this.height + row;

    if (col >= this.width)
      throw new Error(
        `col [${col}] out of range [-${this.width}, ${this.width - 1}]`,
      );
    if (row >= this.height)
      throw new Error(
        `row [${row}] out of range [-${this.height}, ${this.height - 1}]`,
      );

    if (this.bitDepth <= 8 && this.image instanceof Buffer) {
      return this.image[row * this.width + col];
    } else if (this.image instanceof Buffer) {
      const index: number = row * this.width * 2 + col * 2;

      return (this.image[index] << 8) + this.image[index + 1];
    } else {
      return this.image[row][col];
    }
  }
}
